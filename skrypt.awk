#!/bin/awk -f
#
# by Pawel Zurowski <pzurowski@pld-linux.org>
#
# Dnia sobota 30 pa�dziernik 2004 12:05, gozda napisa�:
#> Author: gozda                        Date: Sat Oct 30 10:05:11 2004 GMT
#> Module: fortunes-pl                   Tag: HEAD
#> ---- Log message:
#> - Dodane fortunki z kanalu debian.pl (od Bartosza Fenskiego).
#> - Troche drobnych zmian.
#>
#> ---- Files affected:
#> fortunes-pl:
#>    debian.pl (NONE -> 1.1)  (NEW)
#[..]
#> +Feb 09 13:47:54 <Kraju> Pheagator: jakie ja mia�em pi�kne idea�y. krzewi�,
#> +zach�ca�, pomaga�.
#
#trzeba wyciac date i zrobic kosmetyke np. ,,zach�cac'' powinno byc na 
#wysokosci ,,Pheagator'' (jak w innych fortunkach ircowych)
#
#
#
#   i wlasnie to ten skrypt ma robic
#
BEGIN {
	szerokosc=72;
	laczenie=0
}

# jesli wiersz zaczyna sie od *,% badz #, to wypisujemy i zaczynamy od nowa
/^[#%*]/ {
	wypisz_wypowiedz();
	print;
	next;
}

# zastap date znakiem_poczatku_wypowiedzi i zacznij laczyc linie
/^[A-Z].. .. ..:..:../ {
	wypisz_wypowiedz();
	sub(/^[A-Z].. .. ..:..:.. /,"");
	linie=$0;
	laczenie=1;
	next;
}
laczenie == 1 {
	# zdaje sie, ze spacja jest zbedna
	linie=linie $0;
}


function wypisz_wypowiedz(          i,wyrazy,alinia,aszerokosc,spacje,wciecie,niepierwsza){
	# jesli tryb laczenia nie jest aktywny, to wypad
	if(laczenie==0){return;}
	laczenie=0;
	# jesli linia jest wystarczajaco krotka, to wypisujemy ja i wypad
	if(length(linie) <= szerokosc){
		print linie;
		return;
	}
	# dzielimy linie na wyrazy
	split(linie, wyrazy, " ");
	# liczymy ilosc wyrazow
	spacje=linie;gsub(/[^ ]/,"",spacje);
	alinia="";
	aszerokosc=0;
	# liczymy wciecie
	wciecie=match(linie,/>/)+1;
	niepierwsza=0;
	# wypisujemy dostatecznie dlugie wyrazy
	#     tu bedzie problem, jesli wyraz jest dluzszy niz linia...
	for(i=1;i<=length(spacje)+1;i=i+1){
		if(aszerokosc + length(wyrazy[i]) + niepierwsza * wciecie > szerokosc){
			spacefill( niepierwsza * wciecie );
			print alinia;
			aszerokosc=0;
			niepierwsza=1;
		}
		if(aszerokosc == 0){
			alinia=wyrazy[i];
			aszerokosc=aszerokosc + length(wyrazy[i]);
		}else{
			alinia=alinia " " wyrazy[i];
			aszerokosc=aszerokosc + length(wyrazy[i]) + 1;
		}
	}
	if(aszerokosc != 0){
		spacefill( niepierwsza * wciecie );
		print alinia;
	}
}
# domalowywanie n spacji
function spacefill(n){
	while(n>0){
		printf(" ");
		n=n-1;
	}
}

